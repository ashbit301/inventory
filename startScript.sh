#!/bin/sh
set -xe
pip3 install -r requirement.txt
gunicorn -w 4 -b 0.0.0.0:8000 main:app
